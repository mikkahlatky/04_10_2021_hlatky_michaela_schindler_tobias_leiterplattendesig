# ELSYS_Beamer

Dieses Paket ermöglicht die einfache Erstellung von ELSYS Präsentationen nach der Vorlage von Laura Sanna.
Für Anmerkungen zur LaTeX Implementierung oder Wünschen: [martensma55863@th-nuernberg.de](mailto:martensma55863@th-nuernberg.de)

Bisherige Features:

Automatische Umschaltung des Styls zwischen folgenden Optionen:

```
\usetheme[style=XXX, logo=XXX, colorspace=XXX, nologoheader]{ELSYS}
```
style:

1. ELSYS (Default)

2. Masch: Elektrische Maschinen

3. Mech: Mechatronische Systeme

4. Mod: Modellbasierte Systemoptimierung

5. LE: Leistungselektronik

6. ES: Embedded Systems

logo (Default: Same as style):

1. ELSYS 

2. Masch: Elektrische Maschinen

3. Mech: Mechatronische Systeme

4. Mod: Modellbasierte Systemoptimierung

5. LE: Leistungselektronik

6. ES: Embedded Systems

colorspace (Default: RGB):

1. RGB( For Screens)

2. CMYK (For better print results)


nologoheader:

1.	option set: No logo in header (recomended for ELSYS Style)

2.	not set: Logo appears at top right